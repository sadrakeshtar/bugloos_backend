<?php

namespace Feed\System;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Feed\System\Providers\PackageServiceProvider;

class FeedServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Router $router)
    {

        $this->registerClassServices();
        $this->configLocalType();

//        if (class_exists(JwtHandler::class)) {
//
//            $router->aliasMiddleware('jwt_handler', JwtHandler::class);
//        }
//        if (class_exists(AclHandler::class)) {
//            $router->aliasMiddleware('acl_handler', AclHandler::class);
//        }

    }

    public function registerClassServices()
    {
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $this->app->register(PackageServiceProvider::class);

    }

    public function configLocalType()
    {
        $local = config('feed.local');
        if ($local == 'fa') {
            App::setLocale(config('feed.local'));
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $configs = [
            'feed' => 'feed.php',
            'feed_api' => 'api.php',
        ];
        foreach ($configs as $key => $config) {
            $this->mergeConfigFrom(__DIR__ . '/../config/' . $config, $key);
        }
    }
}
