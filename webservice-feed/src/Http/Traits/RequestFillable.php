<?php

namespace Feed\System\Http\Traits;

use Feed\System\Exceptions\CoreException;

trait RequestFillable
{
    protected function prepareForValidation()
    {
        if (empty($this->rules())) {
            return;
        }

        foreach ($this->keys() as $key) {
            if (! in_array($key, array_keys($this->rules()))) {
                $this->request->remove($key);
            }
        }
    }

}
