<?php


namespace Feed\Packages\user\src\models;

use Core\System\Exceptions\CoreException;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Core\System\Http\Traits\HelperTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Users extends Authenticatable implements JWTSubject
{
    use  HasFactory, Notifiable,HelperTrait;

    protected $fillable = [
        'name',
        'email',
        'mobile_verified_at',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    private static $_instance = null;

    /**
     * @return Users|null
     * make new instance form model to access methods in out of the box
     */
    public static function _()
    {
        if (self::$_instance == null) {
            self::$_instance = new Users();
        }
        return self::$_instance;
    }


    public function register($payload)
    {
        try {
            $payload['password'] =  bcrypt($payload['password']);
            $result = Users::create($payload);

            return $this->modelResponse(['data' => $result]);
        } catch (\Exception $e) {
            return $this->errorHandler($e);
        }
    }
    public function login($payload)
    {

        if (!$token = auth()->attempt($payload)) {
            throw new CoreException('اطلاعات کاربری صحیح نمی باشد',401);
        }
        return $this->createNewToken($token);
    }
    protected function createNewToken($token)
    {
        return $this->modelResponse(['data' => [
            'access_token' => $token,
//            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' => auth('api')->user()
        ]]);
    }
    /**
     * jwt auth implement methods
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
