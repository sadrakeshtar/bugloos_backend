<?php

namespace Feed\Packages\user\src\controllers;

use Feed\Packages\user\src\models\Users;
use Feed\Packages\user\src\request\LoginRequest;
use Feed\Packages\user\src\request\RegisterRequest;

use Feed\System\Http\Controllers\CoreController;


/**
 * Class UserPackageController
 *
 * @package Core\Packages\user\src\controllers
 */
class UserPackageController extends CoreController
{

    private $_register = [
        'name',
        'email',
        'password',
    ];

    private $_login = [
        'email',
        'password'
    ];

    public function index()
    {

        $users = Users::_()->jsonPaginate();
        return $users;
    }

    public function register(RegisterRequest $request)
    {
        $payload = $request->only($this->_register);
        $result = Users::_()->register($payload);
        return $this->responseHandler($result);
    }

    public function login(LoginRequest $request)
    {
        $payload = $request->only($this->_login);
        $result = Users::_()->login($payload);

        return $this->responseHandler($result);
    }



}
