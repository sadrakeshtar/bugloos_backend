<?php

namespace Feed\Packages\user\src\request;

use Feed\System\Http\Requests\FormRequestCustomize ;

class LoginRequest extends FormRequestCustomize
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|max:100',
            'password' => 'required',
        ];
    }
}
