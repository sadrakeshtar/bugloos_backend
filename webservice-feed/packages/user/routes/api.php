<?php

use Feed\Packages\user\src\controllers\UserPackageController;

$prefix = config('feed_api.prefix') . '/users';

Route::group(['prefix' => $prefix], function () {

    Route::group(['middleware' => "jwt_auth"], function () {
        Route::post('/provider-config', [UserPackageController::class, 'provider-config'])->name('feed.users.providers.config');
    });
    Route::post('register', [UserPackageController::class, 'register'])->name('feed.users.register');
    Route::post('login', [UserPackageController::class, 'login'])->name('feed.users.login');
});

