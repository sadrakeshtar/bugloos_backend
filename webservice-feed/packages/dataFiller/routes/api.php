<?php


use Feed\Packages\dataFiller\src\controllers\DataFillerPackageController;

$prefix = config('feed_api.prefix') . '/data-filler';


Route::group(['prefix' => $prefix,'middleware' => "jwt_auth"], function () {

    Route::post('/', [DataFillerPackageController::class, 'store']);

});

