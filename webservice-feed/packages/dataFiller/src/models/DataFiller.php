<?php

namespace Feed\Packages\dataFiller\src\models;

use Feed\System\Http\Traits\HelperTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataFiller extends Model
{
    use HasFactory,HelperTrait;
    protected $fillable = [
        'data_module_name',
        'data_collection',
        'sender_ip',
    ];


    protected $casts = [
        'data_collection' => 'array',
    ];


    private static $_instance = null;

    /**
     * @return DataFiller|null
     * make new instance form model to access methods in out of the box
     */
    public static function _()
    {
        if (self::$_instance == null) {
            self::$_instance = new DataFiller();
        }
        return self::$_instance;
    }


    public function storeData($payload)
    {
        try {
            $result = $this::_()->insertRow($payload);
            return $this->modelResponse(['data' => $result]);
        } catch (\Exception $e) {
            return $this->errorHandler($e);
        }
    }
}
