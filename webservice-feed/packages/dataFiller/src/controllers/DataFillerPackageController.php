<?php

namespace Feed\Packages\dataFiller\src\controllers;



use Feed\Packages\dataFiller\src\models\DataFiller;
use Feed\Packages\dataFiller\src\request\DataFillerStoreRequest;
use Feed\System\Http\Controllers\CoreController;

class DataFillerPackageController extends CoreController
{
    private $_store = [
        'data_module_name',
        'data_collection',
    ];



    public function store(DataFillerStoreRequest $request){

        $payload = $request->only($this->_store);
        $payload['sender_ip'] = $request->ip();

        $result = DataFiller::_()->storeData($payload);
        return $this->responseHandler($result, 'create');
    }
}
