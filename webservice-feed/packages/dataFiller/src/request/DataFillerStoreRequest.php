<?php

namespace Feed\Packages\dataFiller\src\request;

use Core\System\Http\Requests\FormRequestCustomize ;

class DataFillerStoreRequest extends FormRequestCustomize
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data_module_name' => 'required|string|max:20',
            'data_collection' => 'required',
        ];
    }
}
