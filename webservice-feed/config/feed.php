<?php
return [
    'controllers' => [
        'namespace' => 'Feed\\System\\Http\\Controllers'
    ],
    'packages_controllers' => [
        'namespace' => 'Feed\\Packages'
    ],
    'local' => 'fa',
    'packages' => [
        \Feed\Packages\user\UserServiceProvider::class,
        \Feed\Packages\dataFiller\DataFillerServiceProvider::class,
    ],
    'prefix' => 'feed'
];
