<?php

namespace  Core\System\Http\Controllers;

use App\Http\Controllers\Controller;

class BaseController extends Controller
{

    public function responseHandler($data,$type = null)
    {

        $messages = '';
        if (!empty($data->hasError) && $data->hasError == true) {
            $messages = $data->message;
        } else {

            if (!empty($data->error)) {
                $messages = $data->error_description;
            }else{
                if ((isset($data->result->id) or (isset($data->id))) and isset($type) and $type == "create"){
                    return [
                        'code'  => 0,
                        'message' =>"عملیات با موفقیت ثبت شد",
                        'data'      => [
                            "id"=>$data->result->id ?? $data->id
                        ],
                    ];
                }elseif((isset($data->result->id) or (isset($data->id))) and isset($type) and $type == "delete"){
                    return [
                        'code'  => 0,
                        'message' =>"ایتم با موفقیت حذف شد",
                        'data'      => [
                            "id"=>$data->result->id ?? $data->id
                        ],
                    ];
                }elseif((isset($data->result->id) or (isset($data->id))) and isset($type) and $type ==  "update"){
                    return [
                        'code'  => 0,
                        'message' =>"ایتم با موفقیت بروزرسانی شد",
                        'data'      => [
                            "id"=>$data->result->id ?? $data->id
                        ],
                    ];
                }

            }
        }

        if (isset($data->message) and empty($messages) ){
            $messages = $data->message;
        }
        if (!empty($messages) && !is_array($messages)) {
            $messages = [
                $messages
            ];
        }
        $responseRecord = !empty($data->result) ? $data->result : [];
        return response()->json(['data'=>$responseRecord], 200, ['Content-Type' => 'application/json;charset=utf8'],
        JSON_UNESCAPED_UNICODE);
    }


}
