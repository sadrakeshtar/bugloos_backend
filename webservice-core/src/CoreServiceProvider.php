<?php

namespace Core\System;

use core\System\Http\Middleware\AclHandler;
use Core\System\Http\Middleware\JwtAuthCheck;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Core\System\Providers\PackageServiceProvider;

class CoreServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Router $router)
    {

        $this->registerClassServices();
        $this->configLocalType();
        if (class_exists(JwtAuthCheck::class)) {

            $router->aliasMiddleware('jwt_auth', JwtAuthCheck::class);
        }
        if (class_exists(AclHandler::class)) {
            $router->aliasMiddleware('acl_handler', AclHandler::class);
        }

    }

    public function registerClassServices()
    {
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $this->app->register(PackageServiceProvider::class);

    }

    public function configLocalType()
    {
        $local = config('core.local');
        if ($local == 'fa') {
            App::setLocale(config('core.local'));
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $configs = [
            'core' => 'core.php',
            'api' => 'api.php',
        ];
        foreach ($configs as $key => $config) {
            $this->mergeConfigFrom(__DIR__ . '/../config/' . $config, $key);
        }
    }
}
