<?php


use Core\Packages\visualizer\src\controllers\VisualizerPackageController;

$prefix = config('api.prefix') . '/visualizer';
Route::group(['prefix' => $prefix ,'middleware' => "jwt_auth"], function () {
    Route::post('/', [VisualizerPackageController::class, 'store'])->name('visualizer.store');
    Route::get('/', [VisualizerPackageController::class, 'index'])->name('visualizer.index');
    Route::get('/{id}', [VisualizerPackageController::class, 'show'])->name('visualizer.show');
    Route::get('/{id}/data', [VisualizerPackageController::class, 'getModelData'])->name('visualizer.get_data');
    Route::post('{visualizer_id}/user', [VisualizerPackageController::class, 'assignUser'])->name('visualizer.visualizer_user');
});


