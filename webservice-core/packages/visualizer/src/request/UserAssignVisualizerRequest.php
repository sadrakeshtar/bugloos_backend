<?php

namespace Core\Packages\visualizer\src\request;

use Core\Packages\visualizer\src\models\Users;
use Core\System\Http\Requests\FormRequestCustomize ;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;

class UserAssignVisualizerRequest extends FormRequestCustomize
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function validationData()
    {
        return array_merge($this->all(),$this->route()->parameters());
    }


    public function rules()
    {
        return [
            'users' => 'required|exists:users,id',
            'visualizer_id' => 'required|exists:visualizers,id',
        ];
    }
}
