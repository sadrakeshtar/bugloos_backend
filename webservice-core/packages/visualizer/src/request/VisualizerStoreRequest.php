<?php

namespace Core\Packages\visualizer\src\request;

use Core\System\Http\Requests\FormRequestCustomize ;

class VisualizerStoreRequest extends FormRequestCustomize
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:20',
            'label' => 'required|string|max:20',
            'configs.col_headers' => ['required'],
            'configs.col_headers.*.key' => ['required','string'],
            'configs.col_headers.*.label' => ['required','string'],
            'configs.col_headers.*.filterKey' => ['required','string'],
            'configs.col_headers.*.sortable' => ['required','bool'],
            'configs.col_headers.*.filterable' => ['required','bool'],
            'configs.pagination.per_page' => ['required','integer'],
        ];
    }
}
