<?php

namespace core\Packages\visualizer\src\controllers;

use Core\Packages\visualizer\src\models\Users;
use Core\Packages\visualizer\src\models\Visualizer;
use Core\Packages\visualizer\src\request\UserAssignVisualizerRequest;
use Core\Packages\visualizer\src\request\VisualizerStoreRequest;
use Core\System\Http\Controllers\CoreController;


/**
 * Class UserPackageController
 *
 * @package Core\Packages\user\src\controllers
 */
class VisualizerPackageController extends CoreController
{
    private $_store = [
        'name',
        'label',
        'data_model',
        'configs',
    ];

    private $_assign_user = [
        'users',
        'visualizer_id',
    ];

    public function index()
    {
        $user = auth('api')->user();

        $result = Visualizer::_()->getUserVisualizers($user->id);

        return $this->responseHandler($result);
    }


    public function show($id){
        $user = auth('api')->user();
        $result = Visualizer::_()->getUserVisualizers($user->id,$id);
        return $this->responseHandler($result);
    }

    public function getModelData($id){
        $user = auth('api')->user();
        $result = Visualizer::_()->getUserVisualizerData($user->id,$id);
        return $this->responseHandler($result);
    }

    public function store(VisualizerStoreRequest $request)
    {
        $payload = $request->only($this->_store);

        $name = $payload['name'];
        $data_model = $payload['data_model'];
        $label = $payload['label'];
        $configs = $payload['configs'];

        $result = Visualizer::_()->createVisualizer($name,$data_model, $label, $configs);
        return $this->responseHandler($result, 'create');
    }


    public function assignUser($visualizer_id, UserAssignVisualizerRequest $request)
    {

        $payload = $request->only($this->_assign_user);

        $users = $payload['users'];

        $result = Visualizer::_()->assignUser($users, $visualizer_id);

        return self::responseHandler((object)['message' => 'کاربر با موفقیت اضافه شد']);

    }
}
