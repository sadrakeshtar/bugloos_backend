<?php

namespace Core\Packages\visualizer\src\models;

use Core\System\Exceptions\CoreException;
use Core\System\Http\Traits\HelperTrait;
use Feed\Packages\dataFiller\src\models\DataFiller;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Visualizer extends Model
{
    use HasFactory,HelperTrait;

    protected $fillable = [
        'name',
        'data_model',
        'label',
        'configs',
    ];

    protected $casts = [
        'configs' => 'array',
    ];

    private static $_instance = null;

    /**
     * @return Visualizer|null
     * make new instance form model to access methods in out of the box
     */
    public static function _()
    {
        if (self::$_instance == null) {
            self::$_instance = new Visualizer();
        }
        return self::$_instance;
    }

    //relations


    public function users()
    {
        return $this->belongsToMany(Users::class,'visualizer_user','visualizer_id','user_id');
    }

    public function VisualizerData(){
        return $this->hasMany(DataFiller::class,'data_module_name','data_model');
    }


    public function createVisualizer($name,$data_model, $label, $configs)
    {

        return $this->_()->insertRow([
            "name" => $name,
            "label" => $label,
            "data_model" => $data_model,
            "configs" => $configs,
        ]);
    }

    public function assignUser($users,$visualizer_id){
        return $this->find($visualizer_id)->users()->attach($users);
    }

    public function getUserVisualizers($user_id,$id=null){
        try {
            $result = Users::find($user_id)->Visualizers;
            !empty($id) ?? $result = $result->find($id);
            return $this->modelResponse(['data' => $result]);
        } catch (\Exception $e) {
            throw new CoreException('لیستی برای این کاربر یافت نشد');
        }

    }
    public function getUserVisualizerData($user_id,$id){

        try {
            $visualizers_user_ids = Users::find($user_id)->Visualizers->pluck('id')->toArray(); //visualizers ids user can access
            $result = Visualizer::with('VisualizerData')->whereIn('id',$visualizers_user_ids)->get();
            return $this->modelResponse(['data' => $result]);
        } catch (\Exception $e) {
            throw new CoreException('لیستی برای این کاربر یافت نشد');
        }

    }
}
