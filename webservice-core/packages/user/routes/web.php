<?php
use Core\Packages\user\src\controllers\CetegoryPackageController;
use Illuminate\Support\Facades\Route;

$prefix = '/users';

Route::group(['prefix' => $prefix ], function () {
    Route::get('/', [CetegoryPackageController::class, 'index'])->name('users.index');
});
