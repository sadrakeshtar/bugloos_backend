<?php

use Core\Packages\user\src\controllers\UserPackageController;

$prefix_auth_providers = config('api.prefix') . '/auth';
Route::group(['prefix' => $prefix_auth_providers], function () {
    Route::post('/register', [UserPackageController::class, 'register'])->name('users.register');
    Route::post('/login', [UserPackageController::class, 'login'])->name('users.login');
});

$prefix_providers = config('api.prefix') . '/providers';
Route::group(['prefix' => $prefix_providers], function () {
    Route::group(['middleware' => "jwt_auth"], function () {
        Route::post('/provider-config', [UserPackageController::class, 'provider-config'])->name('users.providers.config');
    });
});

