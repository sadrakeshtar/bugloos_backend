<?php

namespace Core\Packages\user\src\request;

use Core\System\Http\Requests\FormRequestCustomize ;

class RegisterRequest extends FormRequestCustomize
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|email|max:100|unique:users,email',
            'password' => 'required|string|confirmed|min:6',
            'name' => 'required|string|min:3|max:100',
        ];
    }
}
